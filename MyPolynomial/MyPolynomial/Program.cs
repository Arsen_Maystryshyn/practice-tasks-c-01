﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPolynomial
{
    class Polynomial
    {
        private int _power;
        private double[] _coefficients;
        public double[] Coefficients
        {
            get
            {
                return _coefficients;
            }
        }

        public int Power
        {
            get
            {
                return _power;
            }

        }

        public Polynomial(int power, params double[] coefficients)
        {
            if (power < 0)
            {
                throw new ArgumentOutOfRangeException("The power must be greater than zero");
            }
            if (coefficients.Length != power + 1)
            {
                throw new ArgumentException("Count of coefficients must equals power+1");
            }
            _coefficients = coefficients;
            _power = power;
        }

        public double PolynomialValue(double argument)
        {
            double result = 0;

            for (int i = 0; i <= Power; i++)
            {
                result += Coefficients[i] * Math.Pow(argument, i);
            }

            return result;
        }

        public static Polynomial operator +(Polynomial first, Polynomial second)
        {
            //Determining the necessary data 
            Polynomial greaterPolynom = first.Power > second.Power ? first : second;
            int lessPower = first.Power > second.Power ? second.Power : first.Power;
            int indxOfGreaterPolynom = greaterPolynom.Power;
            int indxOfLessPolynom = lessPower;

            double[] resultCoefficients = new double[greaterPolynom.Power + 1];

            //Assigning the coefficients of greater polynomial to result array of coefficients, while indices are not the same 
            while (indxOfGreaterPolynom != lessPower)
            {
                resultCoefficients[indxOfGreaterPolynom] = greaterPolynom.Coefficients[indxOfGreaterPolynom];
                indxOfGreaterPolynom--;
            }

            //While indices are the same and greater than zero, add coefficients of every polynomial and assing to result array of coefficient
            while (indxOfGreaterPolynom == indxOfLessPolynom && indxOfGreaterPolynom >= 0)
            {
                resultCoefficients[indxOfGreaterPolynom] = first.Coefficients[indxOfGreaterPolynom] + second.Coefficients[indxOfLessPolynom];
                indxOfGreaterPolynom--;
                indxOfLessPolynom--;
            }

            return new Polynomial(greaterPolynom.Power, resultCoefficients);
        }

        public static Polynomial operator -(Polynomial first, Polynomial second)
        {
            double[] contraryCoefficients = new double[second.Coefficients.Length];

            //Filling the array of contrary coefficient to second polynomial coefficients
            for (int i = 0; i < contraryCoefficients.Length; i++)
            {
                contraryCoefficients[i] = -second.Coefficients[i];
            }

            //Returning the sum between first polynomial and polynomial cotrary to second (result will be substruction)
            return (first + new Polynomial(second.Power, contraryCoefficients));
        }

        public static Polynomial operator *(Polynomial first, Polynomial second)
        {
            Polynomial resultPolynomial = Polynomial.MultOnMonomial(first.Coefficients[first.Power], first.Power, second);

            //Determining result polynomial
            for (int i = first.Power - 1; i >= 0; i--)
            {
                resultPolynomial += Polynomial.MultOnMonomial(first.Coefficients[i], i, second);
            }

            return resultPolynomial;
        }

        private static Polynomial MultOnMonomial(double monomialCoefficient, int monomialPower, Polynomial value)
        {
            double[] resultCoefficients = new double[value.Coefficients.Length];

            int currentPower = value.Power;
            int maxPower = currentPower + monomialPower;

            //Get the array of result coefficients
            for (int i = 0; i <= currentPower; i++)
            {
                resultCoefficients[i] = monomialCoefficient * value.Coefficients[i];
            }

            //The array of result coefficient including zero in needed positions
            double[] neededCoefficients = new double[maxPower + 1];
            //Index in "correctCoefficients" array
            int coeffIndx;

            for (int i = currentPower; i >= 0; i--)
            {
                coeffIndx = monomialPower + i;
                neededCoefficients[coeffIndx] = resultCoefficients[i];
            }

            return new Polynomial(maxPower, neededCoefficients);
        }

        public void Show()
        {
            StringBuilder resultStringBuilder = new StringBuilder();

            resultStringBuilder.AppendFormat($"({ Coefficients[Power]})x^{Power} + ");

            for (int i = Coefficients.Length - 2; i > 0; i--)
            {
                resultStringBuilder.AppendFormat($"({ Coefficients[i]})x^{i} + ");
            }

            resultStringBuilder.AppendFormat($"{ Coefficients[0]}");

            Console.WriteLine(resultStringBuilder);
        }

        public void Show(out string resultString)
        {
            StringBuilder resultStringBuilder = new StringBuilder();


            resultStringBuilder.AppendFormat($"({ Coefficients[Power]})x^{Power} + ");

            for (int i = Coefficients.Length - 2; i > 0; i--)
            {
                resultStringBuilder.AppendFormat($"({ Coefficients[i]})x^{i} + ");
            }

            resultStringBuilder.AppendFormat($"{ Coefficients[0]}");

            resultString = resultStringBuilder.ToString();
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                double[] coefficients1 = new double[] { 3, -2, 1,9 };
                double[] coefficients2 = new double[] { 2, 1, 4, -8, 2 };
                Polynomial polynomial1 = new Polynomial(3, coefficients1);
                Polynomial polynimial2 = new Polynomial(4, coefficients2);
                Polynomial resPollynomial = new Polynomial(4, new double[5]);

                Console.WriteLine("Polynumial 1:");
                polynomial1.Show();
                Console.Write("\n\n");

                Console.WriteLine("Polynumial 2:");
                polynimial2.Show();
                Console.Write("\n\n");

                double x = 1;
                Console.WriteLine($"x = {x}");
                double polinomialValueResult = polynomial1.PolynomialValue(x);
                string polynomial;
                polynomial1.Show(out polynomial);
                string resultString = string.Format(polynomial + $" = {polinomialValueResult}");
                Console.WriteLine(resultString+"\n\n");

                Console.WriteLine("Polynomial 1 + Polynomial 2:");
                resPollynomial = polynomial1 + polynimial2;
                resPollynomial.Show();
                Console.Write("\n\n");

                Console.WriteLine("Polynomial 1 - Polynomial 2:");
                resPollynomial = polynomial1 - polynimial2;
                resPollynomial.Show();
                Console.Write("\n\n");

                Console.WriteLine("Polynomial 1 * Polynomial 2:");
                resPollynomial = polynomial1 * polynimial2;
                resPollynomial.Show();
                Console.Write("\n\n");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
